/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var requestHttp = require("request");
var underscore_string = require("underscore.string");
var os = require('os');
var ifaces = os.networkInterfaces();
var bcrypt = require('bcrypt');

var AuthController = {

    login: function (req, res) {
        var username = req.body.username;
        var password = req.body.password;
        var type = req.body.type;
        sails.log.debug(Date.now('Y-m-d'));
        if (username && password){
            Users.findOne({ username: username })
                .exec(function(err, user){
                    if(err){
                       return res.status(403).send('Invalid email or password.');
                    }else if(!user){
                       return res.status(403).send('Invalid email or password.');
                    }

                    bcrypt.compare(password, user.password, function(error, isMatch){
                        if(!isMatch){
                            return res.status(500).send({error: 'Email and password combination do not match'});
                        }

                        AuthController.generateSession(req, user, function(err, sessionPayLoad){
                            if(err){
                                return res.status(400).send(err)
                            }

                            return res.ok(sessionPayLoad);
                        });

                    });
                });
        }else{
            return res.badRequest('Email or password cannot be empty');
        }

    },

    generateSession: function(req, user, callback){
        if(!user){
            return callback({status: 500, msg: "Unable to generate new session, user is null of undefined"});
        }

        var metadata = req.headers;

        Sessions.create({
            userId: user.id,
            refreshToken : Users.generateToken(),
            accessToken : Users.generateToken(),
            appType : req.appType,
            metadata : metadata
        })
        .meta({fetch: true})
        .exec(function (err, createdSession){
            if(err){
                return callback(err);
            }
            
            Sessions.update({userId : createdSession.userId, accessToken : {"!=": createdSession.accessToken/*, createdAt: new Date(Date.now('Y-m-d'))*/}})
                .set({isDeleted: 1})
                .exec(function(err){
                    if(err){
                        return callback(err);
                    }

                    var session = {
                        userId : createdSession.userId,
                        firstname : user.firstname,
                        lastname : user.lastname,
                        refreshToken : createdSession.refreshToken,
                        accessToken : createdSession.accessToken
                    }

                    callback(null, session);
                });
        });
    }    

};

module.exports = AuthController;

