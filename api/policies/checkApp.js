module.exports = function(req, res, next) {
  if (sails.config.appList.indexOf(req.headers["x-app"]) === -1 &&
      sails.config.hosts.indexOf(req.hostname) === -1) {
      sails.log.error("App:", req.headers["x-app"], "not allowed.");
      return res.serverError("Invalid app");
  }

  req.appType = req.headers["x-app"];
  next();
};