/**
 * Users.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

var sha256 = require("crypto-js/sha256");
var bcrypt = require("bcrypt");
var randomToken = require("random-token").create("0123456789");

module.exports = {
  schema: true,
  fetchRecordsOnUpdate: true,
  tableName: 'users',
  attributes: {
    email: {
      type: 'string',
      required: true
    },
    username: {
      type: 'string',
      required: true
    },
    password: {
      type: 'string',
      required: true
    },
    firstname: {
      type: 'string',
      required: true
    },
    lastname: {
      type: 'string',
      required: true
    },
    isOnline: {
      type: "number",
    },
    // createdAt: {
    //   type: "string",
    //   defaultsTo: new Date(Date.now())
    // },
    // updatedAt: {
    //   type: "string",
    //   defaultsTo: new Date(Date.now())
    // }

  },
  
  generateToken : function(numbersOnly) {
    if (numbersOnly) {
      return randomToken(10);
    }

    return sha256(Math.floor(Math.random() * 1000) + "" + new Date().getTime()) + "";
  },

};

