
module.exports = {
  schema : true,

  attributes: {
    userId : {
      model : "users",
      required : true
    },

    refreshToken : {
      type : "string",
      required : true
    },

    accessToken : {
      type : "string",
      required : true
    },

    expiresAt : {
      type : "string",
      defaultsTo : '123089128389012'
    },

    appType : {
      type : "string",
      isIn : sails.config.appList,
      required : true
    },

    metadata : {
      type : "json"
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    }, 

    
  },
};